package com.mars.qywx.service;

import com.mars.qywx.config.WxCpConfiguration;
import com.mars.qywx.config.WxCpProperties;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.cp.api.WxCpMessageService;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.bean.WxCpOauth2UserInfo;
import me.chanjar.weixin.cp.bean.WxCpUser;
import me.chanjar.weixin.cp.bean.message.WxCpMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TestService {

    @Autowired
    private WxCpProperties wxCpProperties;

    public String testOath2(String url) {
        //获取WxCpService
        WxCpProperties.AppConfig appConfig = wxCpProperties.getAppConfigs().get(0);
        final WxCpService wxCpService = WxCpConfiguration.getCpService(appConfig.getAgentId());

        //生成调url地址
        String s = wxCpService.getOauth2Service().buildAuthorizationUrl(url,"snsapi_base","STATE");
        System.out.println(s);
        return s;
    }

    public String getUserId(String code) {
        //获取WxCpService
        WxCpProperties.AppConfig appConfig = wxCpProperties.getAppConfigs().get(0);
        final WxCpService wxCpService = WxCpConfiguration.getCpService(appConfig.getAgentId());

        //根据code获取访问人员信息
        WxCpOauth2UserInfo res = new WxCpOauth2UserInfo();
        try {
            res = wxCpService.getOauth2Service().getUserInfo(code);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        //获取访问用户ID
        String userId = res.getUserId();
        System.out.println(res);
        return userId;
    }


}
