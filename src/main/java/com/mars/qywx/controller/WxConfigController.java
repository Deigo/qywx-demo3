package com.mars.qywx.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 *js-sdk域名验证
 */
@Controller
@RequestMapping("/")
public class WxConfigController {
    /**
     * js-sdk域名验证
     * @return
     */
    @GetMapping("WW_verify_B17OtIiDI4mCaCKM.txt")
    @ResponseBody
    private String returnConfigFile() {
        //把MP_verify_xxxxxx.txt中的内容返回
        System.out.println("正在验证js-sdk域名");
        return "B17OtIiDI4mCaCKM";
    }
}
