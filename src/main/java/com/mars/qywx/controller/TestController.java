package com.mars.qywx.controller;

import com.mars.qywx.service.TestService;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.cp.bean.WxCpUser;
import org.dom4j.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("qywx/test")
public class TestController {

    @Autowired
    private TestService testService;

    /**
     *
     * @param url 回调地址
     * @return
     */
    @GetMapping("Oauth2")
    public String testOauth2(@RequestParam("url")String url){
        String s = this.testService.testOath2(url);
        //重定向 处理回调函数
        return "redirect:"+s;
    }

    /**
     * 回调url处理
     * @param code
     * @return
     */
    @GetMapping("GetUserId")
    public ResponseEntity<String> testGetUserId(@RequestParam String code){
        String userId = this.testService.getUserId(code);
        if(userId == "") {
            //用户不存在，响应404
            return ResponseEntity.notFound().build();
        }
        System.out.println(userId);
        //响应200
        return ResponseEntity.ok(userId);
    }

}
